var mongoose = require("mongoose"),
    passportLocalMongoose = require("passport-local-mongoose");

var UserSchema = new mongoose.Schema({
    username:String,
    password:String,
    email: {type: String, unique: true, required: true},
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    isAdmin:{type:Boolean, default:false},
    isShopkeeper:{typeof:Boolean,default:false},
    name:String,
    address:String,
    paytm:Number
});

UserSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model("User",UserSchema);